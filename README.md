# Recherches dans l'API grâce à PostMan

La première chose que j'ai réalisé à été de lire la doumentation de l'API.

Par la suite j'ai refléchis à ce qu'il me fallait comme informations et j'ai donc utilisé PostMan qui permet de réalisé des
requettes à une API simplement.

# Réalisation des controlleurs

## Bundle utilisés

- symfony maker bundle
```shell
composer require --dev symfony/maker-bundle
```

- symfony http-client
```shell
composer require symfony/http-client
```

## Créations de controlleurs 

Pour créer les controlleurs j'utilise le maker bundle qui permet à l'aide d'une commande de créer simplement un controlleur.
```shell
php bin/console make:controller
```

Initialement je souhaitais mettre toutes mes routes dans un seul fichier. Voyant que seules certain chargement de routes était déjà lent, j'ai changer mon approche et j'ai donc 4 controlleur.
Un pour la gestion de la récupération des données et les trois autres pour les trois routes et vues.

### ApiController

L'API est la pour heberger la fonction qui permet, scomme dit précedement, de récupérer les données provenant de l'API.
```shell
php bin/console make:controller ApiController
```
Le controlleur ne contient qu'une fonction "getData()" qui renvoye un tableau de données.

### Ranking

J'ai choisi de faire ranking en premier car c'est celui qui me parraissait le plus facile à réaliser.
```shell
php bin/console make:controller RankingController
```
L'unique difficulté de ce controlleur à été de récuperer le logo de chaque équipe.

### Ranking5 (Partie 1)

J'ai ensuite faire ranking5 puisque ce n'était qu'une modification de ranking.
```shell
php bin/console make:controller Ranking5Controller
```
J'ai donc réalialiser les différents cas de figures pour que le classement soit correctement afficher puisque lors que Tours FC n'a personne au dessus avec +2 ou personne en dessous avec -2 l'affichage est différent.

Malheuresement je me suis rendu compte de deux choses lors de l'écriture de ce README :
- je n'ai pas balayé tout les cas de figure dans mon code, j'ai uniquement réalisé pour quand Tours est dans le mileu du classement, 1er ou dernier, pas dans les cas ou il est 2ème ou avant-dernier
- deuxième chose j'ai uniquement fait les conditions pour 14 équipes et non pour plus ou moins.

### Calendar

Je me suis en suite atelé à réaliser le calendrier.
```shell
php bin/console make:controller CalendarController
```
Je me suis rendu compte lors de la réalisation que les matchs n'était pas trier par date et par heure.
J'ai donc du trier le tableau d'abord par date puis par l'heure. Le code fonctionne mais uniquement si l'heure n'a pas de minutes.

### Ranking5 (Partie 2)

J'ai donc terminer par le deuxième partie de ranking5 qui à été d'ajouter les matchs de coupe de France de Tours.



