<?php

namespace App\Controller;

use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class CalendarController extends AbstractController
{
    /**
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     */
    #[Route('/calendar', name: 'app_calendar')]
    public function getCalendar(): Response
    {
        $data = ApiController::getData("https://api-dofa.fff.fr/api/compets/407672/phases/1/poules/3/poule_journees?details[]=pouleJourneeWithMatch");

        $calendar = [];
        $matchs = [];

        foreach ($data['hydra:member'] as $item) {
            foreach ($item["matchs"] as $match) {
                $date = new DateTime($match["date"]);

                $timeParts = explode("h", $match["time"]);
                $hour = intval($timeParts[0]);
                $minutes = count($timeParts) > 1 ? intval($timeParts[1]) : 0;


                $matchs[] = [
                    "ex_time" => $hour * 60 + $minutes,
                    "time" => $match["time"],
                    "date" => $date->format('d F Y'),
                    "home" => [
                        "name" => $match["home"]["short_name"],
                        "score" => $match["home_score"],
                        "logo" => ApiController::getData("https://api-dofa.fff.fr{$match["home"]["club"]["@id"]}")["logo"]
                    ],
                    "away" => [
                        "name" => $match["away"]["short_name"],
                        "score" => $match["away_score"],
                        "logo" => ApiController::getData("https://api-dofa.fff.fr{$match["away"]["club"]["@id"]}")["logo"]
                    ]
                ];
            }

            /**
             * @todo repair time function bc i doesn't take min
             */
            usort($matchs, function ($a, $b) {
                if ($a["date"] == $b["date"]) {
                    return strcmp($a["ex_time"], $b["ex_time"]);
                }
                return strcmp($a["date"], $b["date"]);
            });

            $calendar [] = [
                "journee" => $item["number"],
                "matchs" => $matchs
            ];
            $matchs = [];
        }

        return $this->render('api/calendar.html.twig', [
            'apiData' => $calendar,
        ]);
    }

}
