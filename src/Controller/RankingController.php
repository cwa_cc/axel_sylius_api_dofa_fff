<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class RankingController extends AbstractController
{
    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    #[Route('/ranking', name: 'app_ranking')]
    public function getRanking(): Response
    {
        $data = ApiController::getData('https://api-dofa.fff.fr/api/compets/407672/phases/1/poules/3/classement_journees');

        $teams = [];

        foreach ($data['hydra:member'] as $item){
            $teams[] = [
                "name" => $item['equipe']['short_name'],
                "rank" => $item["rank"],
                "points" => $item["point_count"],
                "game_played" => $item["total_games_count"],
                "diff" => $item["goals_diff"],
                "logo" => ApiController::getData("https://api-dofa.fff.fr{$item['equipe']['club']['@id']}")["logo"]
            ];
        }

        return $this->render('api/ranking.html.twig', [
            'apiData' => $teams,
        ]);
    }
}
