<?php

namespace App\Controller;

use DateTime;
use ECSPrefix202306\Symfony\Component\VarDumper\Cloner\Data;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class Ranking5Controller extends AbstractController
{
    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    #[Route('/ranking5', name: 'app_ranking5')]
    public function get5Ranking(): Response
    {
            $data = ApiController::getData('https://api-dofa.fff.fr/api/compets/407672/phases/1/poules/3/classement_journees');

        $teams = [];
        $rank = 0;
        foreach ($data['hydra:member'] as $item){
            if($item['equipe']['short_name'] == "TOURS FC"){
                $rank = $item['rank'];
                $logo = ApiController::getData("https://api-dofa.fff.fr{$item['equipe']['club']['@id']}")["logo"];
            }
        }
        if($rank == 1){
            for($i = $rank;$i < $rank+5;$i++){
                $item = $data['hydra:member'][$i];
                $teams[] = [
                    "name" => $item['equipe']['short_name'],
                    "rank" => $item["rank"],
                    "points" => $item["point_count"],
                    "game_played" => $item["total_games_count"],
                    "wins" => $item["won_games_count"],
                    "draws" => $item["draw_games_count"],
                    "loses" => $item["lost_games_count"],
                    "diff" => $item["goals_diff"],
                    "logo" => ApiController::getData("https://api-dofa.fff.fr{$item['equipe']['club']['@id']}")["logo"]
                ];
            }
        }
        elseif($rank == 14){
            for($i = $rank-5;$i < $rank;$i++){
                $item = $data['hydra:member'][$i];
                $teams[] = [
                    "name" => $item['equipe']['short_name'],
                    "rank" => $item["rank"],
                    "points" => $item["point_count"],
                    "game_played" => $item["total_games_count"],
                    "wins" => $item["won_games_count"],
                    "draws" => $item["draw_games_count"],
                    "loses" => $item["lost_games_count"],
                    "diff" => $item["goals_diff"],
                    "logo" => ApiController::getData("https://api-dofa.fff.fr{$item['equipe']['club']['@id']}")["logo"]
                ];
            }
        }
        else{
            for($i = $rank - 3;$i < $rank+2;$i++){
                $item = $data['hydra:member'][$i];
                $teams[] = [
                    "name" => $item['equipe']['short_name'],
                    "rank" => $item["rank"],
                    "points" => $item["point_count"],
                    "game_played" => $item["total_games_count"],
                    "wins" => $item["won_games_count"],
                    "draws" => $item["draw_games_count"],
                    "loses" => $item["lost_games_count"],
                    "diff" => $item["goals_diff"],
                    "logo" => ApiController::getData("https://api-dofa.fff.fr{$item['equipe']['club']['@id']}")["logo"]
                ];
            }
        }

        $data = ApiController::getData("https://api-dofa.fff.fr/api/compets/405935/phases/1/poules/3/poule_journees?details[]=pouleJourneeWithMatch");
        $matchs = [];
        foreach ($data['hydra:member'] as $item){
            foreach ($item["matchs"] as $match){
                if ($match["home"]["short_name"] == "TOURS FC" or $match["away"]["short_name"] == "TOURS FC"){
                    $date = new DateTime($match["date"]);
                    $matchs[] = [
                        "rank" => $item["poule"]["stage_number"],
                        "date" => $date->format("d F Y"),
                        "time" => $match["time"],
                        "home" => [
                            "name" => $match["home"]["short_name"],
                            "logo" => $match["home"]["club"]["logo"],
                            "score" => $match["home_score"]
                        ],
                        "away" => [
                            "name" => $match["away"]["short_name"],
                            "logo" => $match["away"]["club"]["logo"],
                            "score" => $match["away_score"]
                            ]];
                }
            }
        }

        return $this->render('api/ranking5.html.twig', [
            'apiData' => $teams, 'fr' => $matchs
        ]);
    }
}
