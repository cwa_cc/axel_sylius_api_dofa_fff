<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Calendar2Controller extends AbstractController
{
    #[Route('/calendar2', name: 'app_calendar2')]
    public function index(): Response
    {
        return $this->render('api/calendar2.html.twig');
    }
}
